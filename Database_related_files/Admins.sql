
-- CREATE DB
IF(db_Id(N'CTSim') IS NULL)
    BEGIN
    CREATE DATABASE CTSim
    END

-- Change to CTSim DB
USE CTSim;

-- Admins is a reserved word
CREATE TABLE Admins (
    Id NVARCHAR(50) NOT NULL PRIMARY KEY,
    Firstname NVARCHAR(50) NOT NULL,
    Lastname NVARCHAR(50) NOT NULL,
    Email NVARCHAR(50) NOT NULL
);
insert into Admins (Id, Firstname, Lastname, Email) values ('Hortense', 'SherIdan', 'Waye', 'swaye0@odnoklassniki.ru');
insert into Admins (Id, Firstname, Lastname, Email) values ('Gabi', 'Hardy', 'Mengo', 'hmengo1@angelfire.com');
insert into Admins (Id, Firstname, Lastname, Email) values ('Elianore', 'Nappy', 'Skillicorn', 'nskillicorn2@scribd.com');
insert into Admins (Id, Firstname, Lastname, Email) values ('Darcie', 'Jorie', 'Staite', 'jstaite3@yelp.com');
insert into Admins (Id, Firstname, Lastname, Email) values ('Glory', 'Tracee', 'Skade', 'tskade4@oracle.com');
insert into Admins (Id, Firstname, Lastname, Email) values ('Britteny', 'Herc', 'Haysar', 'hhaysar5@un.org');
insert into Admins (Id, Firstname, Lastname, Email) values ('Ardra', 'Rhody', 'Imlin', 'rimlin6@comsenz.com');
insert into Admins (Id, Firstname, Lastname, Email) values ('Zaccaria', 'Mohammed', 'McLenaghan', 'mmclenaghan7@si.edu');
insert into Admins (Id, Firstname, Lastname, Email) values ('Nathanil', 'Winny', 'Gajewski', 'wgajewski8@eventbrite.com');
insert into Admins (Id, Firstname, Lastname, Email) values ('marcus', 'marcus', 'jhuo', 'foobar@wildworld.com');
