
-- CREATE DB
IF(db_id(N'CTSim') IS NULL)
    BEGIN
    CREATE DATABASE CTSim
    END

-- Change to CTSim DB
USE CTSim;


CREATE TABLE Patients (
    Id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    FirstName NVARCHAR(255) NOT NULL,
    LastName NVARCHAR(255) NOT NULL,
    Decription NVARCHAR(50),
    Birthdate date,
    -- Male, Female, Other
    Sex NVARCHAR(10),
    Address NVARCHAR(255),
    Postcode NVARCHAR(10),
    Indication NVARCHAR(255),
    Question NVARCHAR(255),
    Type NVARCHAR(20),
    ExamType NVARCHAR(20)
);



INSERT INTO CTSIM.dbo.Patients
(FirstName, LastName, Decription, Birthdate, Sex, Address, Postcode, Indication, Question, Type, ExamType)
VALUES ('Beatrice', 'Grice', 'CT of the head', '19430612', 'Female', '35 High Street', 'NT22 1PQ', 'Recurrent daily headaches since March', 'Intracranial pathology causing headaches', 'tutorial', 'Helical'),
('James', 'Melanier', 'CT of the head', '19330215', 'Male', '22 Priory Cres', 'MT1 3PK', 'Admitted with seizures. Not known epileptic. EEG NAD', 'Space occupying lesion', 'practice', 'Helical'),
('Karen', 'Needle', 'CT of the head', '19760303', 'Female', '28 Brook Avenue', 'WD2 4EX', 'Pins + needles in face for 3/12. Was intermittent', 'Intracerebral pathology', 'practice', 'Helical'),
('Martin', 'Shaw', 'CT of the head', '19940404', 'Male', 'Flat 2', '', 'RTA @ 21:00 vomited x6. Headache. CNS altered.\n Trigemmal Sensasion loss of power no neck pain.', 'Bleed', 'practice', 'Sequential'),
('Sally', 'Turner', 'CT of the head', '19760312', 'Female', '14 Calder Rise', 'PT3 6RG', 'Woke with severe headache. Not reduced by analgesia ?Photophobia. CT prior to lumbar puncture.', 'SAH', 'practice', 'Sequential'),
('William Roberts', 'Jones', 'CT of the head', '19290219', 'Male', 'The Meadows', '', 'Patient felt dizzy and collapsed. \n?LOC ?How long. Paramedics noted left sided weakness and facial droop now resolved.', 'Stroke? Bleed', 'practice', 'Sequential'),
('Margaret', 'Bryan', 'CT of the head', '19401201', 'Female', 'The Sycamores', 'South Town', ' checked', 'Intercranial bleed', 'practice', 'Helical')
