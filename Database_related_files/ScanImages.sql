﻿
-- CREATE DB
IF(db_id(N'CTSim') IS NULL)
    BEGIN
    CREATE DATABASE CTSim
END

-- Change to CTSim DB
USE CTSim;

CREATE TABLE [ScanImages] (
    [Id] nvarchar(255) NOT NULL,
    [Kv] int NOT NULL,
    [Mas] nvarchar(50) NOT NULL,
    [ScanTime] nvarchar(50) NOT NULL,
    [SliceThickness] decimal NOT NULL,
    [ScanFov] nvarchar(50) NOT NULL,
    [CtdDose] decimal NOT NULL,
    [DlpDose] decimal NOT NULL,
    [ImageGroup] nvarchar(50) NOT NULL,
    [ImageSeries] nvarchar(50) NOT NULL,
    [ImageURL] nvarchar(255) NOT NULL,
    CONSTRAINT [PK_ScanImage] PRIMARY KEY ([Id])
);


insert into [ScanImages] ([Id], [Kv], [Mas], [ScanTime], 
[SliceThickness], [ScanFov], [CtdDose], [DlpDose], [ImageGroup], [ImageSeries], [ImageURL])
values ('dfgft454',80, '0-149', 'Full 0.5s', 1, '10-19cm', 4.57, 76.72, '80/100', 'Series 4[144]','http://foo.bar/1.jpg');

