﻿USE [master]
GO
DROP DATABASE IF EXISTS [CTSim]

/****** Object:  Database [CTSim]    Script Date: 13/04/2020 1:18:02 PM ******/
CREATE DATABASE [CTSim]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CTSim', FILENAME = N'D:\Program Files\SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\CTSim.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CTSim_log', FILENAME = N'D:\Program Files\SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\CTSim_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [CTSim] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CTSim].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CTSim] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CTSim] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CTSim] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CTSim] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CTSim] SET ARITHABORT OFF 
GO
ALTER DATABASE [CTSim] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CTSim] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CTSim] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CTSim] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CTSim] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CTSim] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CTSim] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CTSim] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CTSim] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CTSim] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CTSim] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CTSim] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CTSim] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CTSim] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CTSim] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CTSim] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CTSim] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CTSim] SET RECOVERY FULL 
GO
ALTER DATABASE [CTSim] SET  MULTI_USER 
GO
ALTER DATABASE [CTSim] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CTSim] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CTSim] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CTSim] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CTSim] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CTSim', N'ON'
GO
ALTER DATABASE [CTSim] SET QUERY_STORE = OFF
GO
USE [CTSim]
GO
/****** Object:  Table [dbo].[Exam]    Script Date: 13/04/2020 1:18:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exam](
	[id] [int] NOT NULL,
	[protocol] [tinyint] NOT NULL,
	[description] [nvarchar](255) NOT NULL,
	[indication] [nvarchar](max) NOT NULL,
	[question] [nvarchar](255) NOT NULL,
	[clinician] [nvarchar](255) NOT NULL,
	[date] [date] NULL,
	[comment] [nvarchar](255) NULL,
	[onwardReferral] [tinyint] NOT NULL,
	[assessmentType] [tinyint] NOT NULL,
	[signed] [bit] NOT NULL,
	[justified] [bit] NOT NULL,
	[imagingProtocol] [bit] NOT NULL,
	[imagingChecked] [bit] NOT NULL,
 CONSTRAINT [PK_Exam] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 13/04/2020 1:18:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient](
	[id] [int] NOT NULL,
	[comID] [nvarchar](255) NOT NULL,
	[firstName] [nvarchar](255) NOT NULL,
	[lastName] [nvarchar](255) NOT NULL,
	[birthdate] [date] NOT NULL,
	[address] [nvarchar](255) NULL,
	[postcode] [nvarchar](255) NULL,
	[sex] [tinyint] NOT NULL,
	[exam] [int] NOT NULL,
 CONSTRAINT [PK_Patients] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (1, 0, N'CT of the head', N'Recurrent daily headaches since March, no improvement.\nWaking from sleep. No neurology or vomiting.', N'Intracranial pathology causing headaches', N'P.Shepherd', CAST(N'2016-10-11' AS Date), N'', 1, 1, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (2, 0, N'CT of the head', N'Admitted with seizures. Not known epileptic. EEG NAD', N'Space occupying lesion', N'M.Mohammed', CAST(N'2016-10-01' AS Date), N'', 2, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (3, 0, N'CT of the head', N'Pins + needles in face for 3/12. Was intermittent, now constant. Intermittent numbness and weakness in right arm.\n Stumbling to left side and some double-vision. Neuro exam, NAD', N'Intracerebral pathology', N'G.Bailey', CAST(N'2016-09-22' AS Date), N'', 1, 2, 0, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (4, 0, N'CT of the head', N'RTA @ 21:00 vomited x6. Headache. CNS altered.\n Trigemmal Sensasion loss of power no neck pain.', N'Bleed.', N'R.Mash', CAST(N'2015-06-19' AS Date), N'', 1, 2, 0, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (5, 0, N'CT of the head', N'Woke with severe headache. Not reduced by analgesia ?Photophobia. CT prior to lumbar puncture.', N'SAH', N'J.Price', CAST(N'2016-09-26' AS Date), N'', 1, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (6, 0, N'CT of the head', N'Patient felt dizzy and collapsed. \n?LOC ?How long. Paramedics noted left sided weakness and facial droop now resolved.', N'Stroke? Bleed.', N'A.B.Davies', CAST(N'2016-02-10' AS Date), N'', 1, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (7, 0, N'CT of the head', N'Witnessed fall in nursing home ?LOC. No nausea/vomiting', N'Intercranial bleed.', N'Smith', CAST(N'2016-09-24' AS Date), N'', 1, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (8, 0, N'', N'Headache today - occipital - says worst ever. Some left sided facial numbness.\n Not like her migraine pain as this was sudden onset.\n Complaining of pain in nape of neck too. ', N'Subarachnoid haemorrhage', N'', NULL, N'', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (9, 0, N'', N'6 months worsening headache behind eyes. 6-12 month history of pain in back of neck/base of skull.\n Happens at same time as frontal headache. Worse in morning and fades slowly. Worse when standing.\n No visual problems. Cranial nerve exam normal. Blood pressure normal.', N'Benign intracranial hypertension.', N'', NULL, N'', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (10, 0, N'', N'Recurrent episodes of loss of consciousness at rest. Attendance at A&E today for two episodes\n in last 24 hours. Non responsive for a few minutes each time.\n Intermittent frontal headches. No limb jerking.', N'Pathology', N'', NULL, N'', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (11, 0, N'', N'Slurred speech. 3 months tiredness. Unable to write properly.\n Family anxious.', N'Stroke ?TIA', N'', NULL, N'', 3, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (12, 0, N'', N'States at times can not remember things. Worsening memory and mobility.', N'Cause of memory problem', N'', NULL, N'Pacemaker', 3, 1, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (13, 1, N'', N'Reccurent issues with balance. Initially requested MRI but unable to tolerate.\n Falls with unsteadiness.', N'Infarction? Subdurals', N'', NULL, N'Claustrophobia', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (14, 0, N'', N'Memory loss. Forgetting conversations and poor short term memory. Unusually short tempered and misplacing things', N'Cause for symptoms', N'', NULL, N'', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (15, 0, N'', N'Complaining of occipital headaches for 2 months.\n Daily afternoon/evening associated with nausea.\n Previous stroke', N'Intracranial mass', N'', NULL, N'Recent MRI head and whole spine showed stable appearances but symptoms persist 6/12 later', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (16, 0, N'', N'Patient ''rolled car'' in accident one month ago. \n Since then persistant headache - especially occipital and difficulty processing thoughts.\n No loss of consciousness or bruising sustained in accident.', N'any intracranial event to explain confusion', N'', NULL, N'', 3, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (17, 0, N'', N'Persistant daily headaches. Tried migraine prophylaxis with no improvement.\n Urgent CT to evaluate please.', N'Intracranial pathology or mass', N'', NULL, N'', 3, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (18, 0, N'', N'Woke at 03:00 today with left facial droop, /n left arm and leg weakness and slurred speech. Suitable for thrombolysis straight from ambulance service', N'Stroke', N'', NULL, N'', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (19, 1, N'', N'RTC. Patient knocked from bicycle. No vomitting. No LOC.', N'bleed, ?fracture', N'', NULL, N'', 1, 3, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (20, 1, N'', N'Pedestrian. Hit by car. Altered sensation left arm. No headache. No LOC', N'bleed, ?fracture', N'', NULL, N'', 1, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (21, 1, N'', N'RTC. Backend Collision. Air cushion released. LOC 3 mins approx (witness account).\n Now complains of pain and altered sensasion right side head/neck.', N'bleed,', N'', NULL, N'', 1, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (22, 1, N'', N'RTC @ 16.00. No witnesses. Complains of loss and visual acuity left eye and headache.', N'bleed,', N'', NULL, N'', 1, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (23, 1, N'', N'Fell from ladder. No LOC. Complains of altered sensation and pain left side of face and neck.', N'bleed, ?fracture', N'', NULL, N'', 1, 2, 1, 1, 1, 1)
INSERT [dbo].[Exam] ([id], [protocol], [description], [indication], [question], [clinician], [date], [comment], [onwardReferral], [assessmentType], [signed], [justified], [imagingProtocol], [imagingChecked]) VALUES (24, 1, N'', N'Fell down stairs at night club. Speech slurred. Apparent pain in head and neck.', N'bleed,', N'', NULL, N'', 1, 1, 1, 1, 1, 1)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (1, N'BD1000', N'Beatrice', N'Grice', CAST(N'1943-06-12' AS Date), N'35 High Street, Northmead', N'NT22 1PQ', 2, 1)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (2, N'BD1437', N'James', N'Melanier', CAST(N'1933-02-15' AS Date), N'22 Priory Cres, Moortown', N'MT1 3PK', 1, 2)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (3, N'BD2250', N'Karen', N'Needle', CAST(N'1976-03-03' AS Date), N'28 Brook Avenue, Walkden', N'WD2 4EX', 2, 3)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (4, N'BD2370', N'Martin', N'Shaw', CAST(N'1994-04-04' AS Date), N'Flat 2, Green Tower', NULL, 1, 4)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (5, N'BD2401', N'Sally', N'Turner', CAST(N'1976-03-12' AS Date), N'14 Calder Rise, Petertown', N'PT3 6RG', 2, 5)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (6, N'BD2533', N'William Robert', N'Jones', CAST(N'1929-02-19' AS Date), N'The Meadows, Drury Lane', NULL, 1, 6)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (7, N'BD2251', N'Margaret', N'Bryan', CAST(N'1940-12-01' AS Date), N'The Sycamores, High Street, South Town', N'S74 1PP', 2, 7)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (8, N'BD5623', N'Tania', N'Norton', CAST(N'1971-10-22' AS Date), N'12 Darby mews, Highborough', N'HB3 7DP', 2, 8)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (9, N'BD3195', N'Louise', N'Ashworth', CAST(N'1984-07-03' AS Date), N'1 Ferry Drive, Fleekford', N'FF4 5BA', 2, 9)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (10, N'BD4962', N'Jessica', N'Hydon', CAST(N'1999-12-04' AS Date), N'66 Ravenswood Close, North Lakes', N'NL7 2LN', 2, 10)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (11, N'BD0094', N'Jeanette', N'Ketting', CAST(N'2041-09-02' AS Date), N'15 Francis Street, Lowmarket', N'LM8 7LE', 2, 11)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (12, N'BD0532', N'Susan', N'Curwen', CAST(N'1956-10-06' AS Date), N'107 Court Street, Westfield', N'WD20 8DA', 2, 12)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (13, N'BD0043', N'Tahira', N'Ahmed', CAST(N'2038-10-12' AS Date), N'25 Normal Drive, Freetown', N'FT6 9WY', 2, 13)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (14, N'BD9472', N'Amy', N'Vernon', CAST(N'1928-07-06' AS Date), N'21 Welsley Close, Garthford', N'GF9 OMW', 2, 14)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (15, N'BD9456', N'Graham', N'Tolley', CAST(N'2034-02-13' AS Date), N'10 Alton Crescent, Westgate', N'WG1 3JM', 1, 15)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (16, N'BD7390', N'Andrew', N'Barlow', CAST(N'1984-06-15' AS Date), N'73 Middleton lane, Parklands Heath', N'PH7 IER', 1, 16)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (17, N'BD3690', N'Hunter', N'Dawson', CAST(N'1986-03-31' AS Date), N'89 Moor View, Wrigglestone', N'WR4 4NA', 1, 17)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (18, N'BD0554', N'Peter Charles', N'Drake', CAST(N'1953-03-18' AS Date), N'143 Waterton place, Bridgeton', N'BG2 5ED', 1, 18)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (19, N'BD5092', N'Harry', N'Jackson', CAST(N'1982-11-17' AS Date), N'23, Holly Court, Leeds', N'LS24, 9PQ', 1, 19)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (20, N'BD672', N'Molly', N'Johnson', CAST(N'1985-03-11' AS Date), N'2, Priesthole Lane, Leeds', N'LS2, 7LT', 2, 20)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (21, N'BD0037', N'Stephen', N'Smith', CAST(N'1985-06-10' AS Date), N'31, Falcon Terrace, Rodley', N'LS14, 2DR', 1, 21)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (22, N'BD01534', N'Lorraine', N'Lafferty', CAST(N'1990-04-01' AS Date), N'101, Main Street, Bradly', N'BD29, 7RG', 2, 22)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (23, N'BD4562', N'Rory', N'McDougal', CAST(N'1982-07-12' AS Date), N'31, Leeds Road, Rodley', N'LS14, 7JA', 1, 23)
INSERT [dbo].[Patient] ([id], [comID], [firstName], [lastName], [birthdate], [address], [postcode], [sex], [exam]) VALUES (24, N'BD7834', N'Deborah', N'Dawson', CAST(N'1989-09-19' AS Date), N'52A, Grange Road, Bradly', N'BD29, 3PQ', 2, 24)
ALTER TABLE [dbo].[Patient]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Exam] FOREIGN KEY([exam])
REFERENCES [dbo].[Exam] ([id])
GO
ALTER TABLE [dbo].[Patient] CHECK CONSTRAINT [FK_Patient_Exam]
GO
USE [master]
GO
ALTER DATABASE [CTSim] SET  READ_WRITE 
GO
