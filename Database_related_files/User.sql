﻿USE [CTSim]
GO

CREATE TABLE [dbo].[Users](
	[Id] [nvarchar](50) NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
	[Organization] [nvarchar](50) NOT NULL,
	[ClassName] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO

insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00u914ptsRl5pz5VH4x6', 'SuperAdmin', 'Organization1', 'Class1');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00u5603a77rLtcBdZ4x6', 'SuperAdmin', 'Organization1', 'Class1');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00u565e9jDDMZFNff4x6', 'SuperAdmin', 'Organization1', 'Class2');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00u913nlehkAtcz7h4x6', 'Admin', 'Organization1', 'Class2');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00u9z2p0npubyHkPB4x6', 'Student', 'Organization1', 'Class1');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00ua23l96OtdfglMy4x6', 'Student', 'Organization2', 'Class1');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00uad8xv9ixaYYwjt4x6', 'Student', 'Organization2', 'Class1');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00uad9l5eXhIaVBmC4x6', 'Teacher', 'Organization1', 'Class2');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00uad9m9v5XjAURbp4x6', 'Student', 'Organization1', 'Class1');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00uadfifwjnjSt5Hw4x6', 'Teacher', 'Organization2', 'Class3');
insert into [Users] ([Id], [Role], [Organization], [ClassName]) values ('00uadg2fzaMczc4dS4x6', 'Organization', 'Organization1', 'Class2');
