﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace CTSim.Controllers
{
    //[Authorize(Policy = "IsAdmin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminsController : ControllerBase
    {
        private readonly CTSimContext context;
        private readonly IHttpClientFactory httpClientFactory;
        private readonly string authtoken;

        public AdminsController(CTSimContext context, IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            this.context = context;
            this.httpClientFactory = httpClientFactory;
            authtoken = configuration.GetSection("OktaToken").Value;
        }

        // GET: api/admins
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Admin>>> GetAdmin()
        {
            return await context.Admins.ToListAsync();
        }

        // GET: api/admins/{id} 
        [HttpGet("{id}")]
        public async Task<ActionResult<Admin>> GetAdmin(string id)
        {
            var user = context.Users.Find(id);


            var admin = await context.Admins.FindAsync(id);

            if (admin == null)
            {
                return NotFound();
            }

            return admin;
        }

        // PUT: api/admins/Gabi
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdmin(string id, Admin admin)
        {
            if (!id.Equals(admin.Id))
            {
                return BadRequest();
            }

            context.Entry(admin).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdminExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/admins

        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Admin>> PostAdmin(Admin admin)
        {
            context.Admins.Add(admin);
            await context.SaveChangesAsync();

            return CreatedAtAction("GetAdmin", new { id = admin.Id }, admin);
        }

        // DELETE: api/admins/Gabi
        [HttpDelete("{id}")]
        public async Task<ActionResult<Admin>> DeleteAdmin(string id)
        {
            var admin = await context.Admins.FindAsync(id);
            if (admin == null)
            {
                return NotFound();
            }

            context.Admins.Remove(admin);
            await context.SaveChangesAsync();

            return admin;
        }

        private bool AdminExists(string id)
        {
            return context.Admins.Any(e => e.Id.Equals(id));
        }
    }
}
