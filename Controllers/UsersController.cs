﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CTSim.Models;
using System.Text;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using CTSim.AuthRequirement;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using CTSim.SQL;
using IdentityServer4.Models;
using Microsoft.CodeAnalysis.CodeFixes;

namespace CTSim.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly CTSimContext context;
        private readonly HttpClient httpClient;
        private readonly int timeout = 20000;
        private Tokensql tokensql;

        public UsersController(CTSimContext context, IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            this.context = context;

            IHttpClientFactory clientFactory = httpClientFactory;
            httpClient = clientFactory.CreateClient();
            string authToken = configuration.GetSection("OktaToken").Value;
            tokensql = new Tokensql();
            httpClient.DefaultRequestHeaders.Add("Authorization", authToken);
            httpClient.Timeout = TimeSpan.FromMilliseconds(timeout);
        }

        // Get: api/users
        [HttpGet]
        public async Task<ActionResult<string>> GetUser()
        {
            //token 
            /*if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }*/

            var request = new HttpRequestMessage(HttpMethod.Get, "https://virtualmedicalcoaching.okta.com/api/v1/users");
            try
            {
                var response = await httpClient.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                throw e;
            }
        }

        // Get: api/users/getalluser
        [HttpGet("getAlluser")]
        public async Task<ActionResult<IEnumerable<User>>> GetAllUserInfo()
        {
            //token 
            /*if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }*/

            return await context.Users.ToListAsync();
        }

        // Get: api/users/getuser/00u9829t8H03A9txd4x6
        [HttpGet("getuser/{userId}")]
        //[Authorize("Teacher")]
        public async Task<ActionResult<User>> GetUserById(string userId)
        {
            //token 
            /*if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }*/

            var user = await context.Users.FindAsync(userId);

            if (user == null)
            {
                return NotFound();
            }

            return user;

        }

        // Get: api/users/getuserName/00u9829t8H03A9txd4x6
        [HttpGet("getuserName/{userId}")]
        [Authorize("Teacher")]
        public async Task<ActionResult<string>> GetUserNameById(string userId)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, "https://virtualmedicalcoaching.okta.com/api/v1/users/" + userId);
            try
            {
                var response = await httpClient.SendAsync(request);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                throw e;
            }

        }

        // POST: api/Users/update  :  update one user
        [HttpPost("update")]
        [Authorize("Teacher")]
        public async Task<ActionResult<string>> UpdateUser()
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            string data = Request.Form["data"];
            var content = new StringContent(data, Encoding.UTF8, "application/json");
            string requestUrl = "https://virtualmedicalcoaching.okta.com/api/v1/users/" + Request.Form["url"];
            try
            {
                var response = await httpClient.PostAsync(requestUrl, content);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                throw e;
            }
        }

        // POST: api/Users/updateUserType  :  update one user
        [HttpPost("updateUserType")]
        [Authorize("Teacher")]
        public async Task<ActionResult> UpdateUserType()
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            User oneUser = new User();
            oneUser.Id = Request.Form["id"];
            oneUser.Role = Request.Form["role"];
            oneUser.ClassName = Request.Form["className"];
            oneUser.Organization = Request.Form["organization"];

            context.Entry(oneUser).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(oneUser.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users/forgot  :  send email to reset password
        [HttpPost("forgot")]
        public async Task<ActionResult<string>> ForgotPassword()
        {
            string data = Request.Form["data"];
            var content = new StringContent(data, Encoding.UTF8, "application/json");
            string requestUrl = "https://virtualmedicalcoaching.okta.com/api/v1/users/" + Request.Form["url"] + "/credentials/forgot_password?sendEmail=true";
            try
            {
                var response = await httpClient.PostAsync(requestUrl, content);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                throw e;
            }
        }


        // Get: api/Users/delete/userid  :  delete one user
        [HttpGet("delete/{userId}")]
        [Authorize("Teacher")]
        public async Task<ActionResult<string>> DeleteUser(string userId)
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            try
            {
                var response = await httpClient.DeleteAsync("https://virtualmedicalcoaching.okta.com/api/v1/users/" + userId);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                throw e;
            }
        }


        //Okta api finish 
        // GET: api/Users/5
        [HttpGet("{id}")]
        [Authorize("Teacher")]
        public async Task<ActionResult<User>> GetUsers(int id)
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            var users = await context.Users.FindAsync(id);

            if (users == null)
            {
                return NotFound();
            }

            return users;
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsers(string id, User users)
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            if (id != users.Id)
            {
                return BadRequest();
            }

            context.Entry(users).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/users
        [HttpPost]
        public async Task<ActionResult<User>> PostUsers()
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            User newUser = new User();
            newUser.Id = Request.Form["id"];
            newUser.Role = Request.Form["role"];
            newUser.Organization = Request.Form["organization"];
            newUser.ClassName = Request.Form["className"];
            Console.WriteLine(newUser.Id);
            Console.WriteLine(newUser.Role);
            context.Users.Add(newUser);
            await context.SaveChangesAsync();

            return CreatedAtAction("GetUsers", new { id = newUser.Id }, newUser);
        }


        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUsers(int id)
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            var users = await context.Users.FindAsync(id);
            if (users == null)
            {
                return NotFound();
            }

            context.Users.Remove(users);
            await context.SaveChangesAsync();

            return users;
        }


        //POST: api/Users/auth
        [HttpPost("auth")]
        public async Task<ActionResult<string>> AuthUser()
        {
            //convert to json
            string logindata = Request.Form["data"];
            string decryptData = Security.AESDecrypt(logindata.ToString());
            var content = new StringContent(decryptData, Encoding.UTF8, "application/json");
            string requestUrl = "https://virtualmedicalcoaching.okta.com/api/v1/authn";
            try
            {
                //get response
                var response = await httpClient.PostAsync(requestUrl, content);
                response.EnsureSuccessStatusCode();
                await response.Content.ReadAsStringAsync();
                JObject resultJson = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                //set token
                tokensql.newToken(resultJson["_embedded"]["user"]["id"].ToString(), resultJson["sessionToken"].ToString());
                //set return value
                JObject returnJson = new JObject { { "Id", resultJson["_embedded"]["user"]["id"].ToString() },
                                           { "Status", resultJson["status"].ToString() },
                                           { "token", resultJson["sessionToken"].ToString()}};
                //create new token 
                
                //return token
                //encrypt
                var result = Security.AESEncrypt(returnJson.ToString());
                return result;
            }
            catch (HttpRequestException e)
            {
                throw e;
            }
        }


        // POST: api/Users/newUser
        [HttpPost("newUser")]
        [Authorize("Teacher")]
        public async Task<ActionResult<string>> newUser()
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            var data = Request.Form["data"];
            var content = new StringContent(data, Encoding.UTF8, "application/json");
            string requestUrl = "https://virtualmedicalcoaching.okta.com/api/v1/users?activate=true";

            try
            {
                var response = await httpClient.PostAsync(requestUrl, content);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException e)
            {
                throw e;
            }
        }
       

        private bool UsersExists(string id)
        {
            return context.Users.Any(e => e.Id == id);
        }
    }
}
