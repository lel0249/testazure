﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using CTSim.Models;

namespace CTSim.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScanImagesController : ControllerBase
    {
        private readonly CTSimContext context;
        private readonly IHttpClientFactory httpClientFactory;

        public ScanImagesController(CTSimContext context, IHttpClientFactory httpClientFactory)
        {
            this.context = context;
            this.httpClientFactory = httpClientFactory;
        }


        // GET: api/scanImages?sliceThickness=1&scanfov=10-19cm
        [HttpGet]
        public async Task<ActionResult<ScanImage>> GetScanImage(int sliceThickness, string scanFov)
        {
            var scanImage = await context.ScanImages
                            .Where(MatchThicknessAndFOV(sliceThickness, scanFov))
                            .ToListAsync();

            if (scanImage == null) { return NotFound(); }

            return scanImage[0];
        }


        private static Expression<Func<ScanImage, bool>> MatchThicknessAndFOV(int sliceThickness, string fov)
        {
            return x => x.SliceThickness == sliceThickness && x.ScanFov == fov;
        }

    }
}