﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CTSim.Models;
using System.Net.Http;
using CTSim.SQL;

namespace CTSim.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientsController : ControllerBase
    {
        private readonly CTSimContext context;
        private readonly IHttpClientFactory httpClientFactory;
        private Tokensql tokensql;

        public PatientsController(CTSimContext context, IHttpClientFactory httpClientFactory)
        {
            this.context = context;
            this.httpClientFactory = httpClientFactory;
            tokensql = new Tokensql();
        }

        // GET: api/Patients
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Patient>>> GetAllPatient()
        {

            return await context.Patients.ToListAsync();
        }

        // Get: api/Patients/studentId/00u9829t8H03A9txd4x6
        [HttpGet("studentId/{studentId}")]
        public async Task<ActionResult<IEnumerable<Patient>>> GetPatientBysStudentId(string studentId)
        {
            var patients = await context.Patients.Where(s => s.StudentId == studentId).ToListAsync();
            return patients;
        }

        // GET: api/Patients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Patient>> GetPatient(int id)
        {
            var patient = await context.Patients.FindAsync(id);

            if (patient == null)
            {
                return NotFound();
            }

            return patient;
        }

        // PUT: api/Patients/update
        [HttpPost("update")]
        public async Task<IActionResult> UpdatePatient()
        {
            //token 
            if (tokensql.testToken(this.Request.Headers["token"]) == false)
            {
                return NotFound();
            }

            Patient onePatient = new Patient();
            onePatient.Id = int.Parse(Request.Form["id"]);
            onePatient.StudentId = Request.Form["StudentId"];
            onePatient.FirstName = Request.Form["FirstName"];
            onePatient.LastName = Request.Form["LastName"];
            onePatient.Decription = Request.Form["Decription"];
            onePatient.Birthdate = Convert.ToDateTime(Request.Form["Birthdate"]);
            onePatient.Sex = Request.Form["Sex"];
            onePatient.Address = Request.Form["Address"];
            onePatient.Postcode = Request.Form["Postcode"];
            onePatient.Indication = Request.Form["Indication"];
            onePatient.Question = Request.Form["Question"];

            context.Entry(onePatient).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PatientExists(onePatient.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Patients
        [HttpPost]
        public async Task<ActionResult<Patient>> PostPatient(Patient patient)
        {
            context.Patients.Add(patient);
            await context.SaveChangesAsync();

            return CreatedAtAction("GetPatient", new { id = patient.Id }, patient);
        }

        // DELETE: api/Patients/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Patient>> DeletePatient(int id)
        {
            var patient = await context.Patients.FindAsync(id);
            if (patient == null)
            {
                return NotFound();
            }

            context.Patients.Remove(patient);
            await context.SaveChangesAsync();

            return patient;
        }

        private bool PatientExists(int id)
        {
            return context.Patients.Any(e => e.Id == id);
        }
    }
}
