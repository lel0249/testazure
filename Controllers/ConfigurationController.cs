﻿using CTSim.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CTSim.Controllers
{
        [Route("api/[controller]")]
        [ApiController]
        public class ConfigurationController : ControllerBase
        {
            private readonly CTSimContext context;
            private readonly IHttpClientFactory httpClientFactory;

            public ConfigurationController(CTSimContext context, IHttpClientFactory httpClientFactory)
            {
                this.context = context;
                this.httpClientFactory = httpClientFactory;
            }

            [HttpGet]
            public async Task<ActionResult<IEnumerable<Configuration>>> GetConfiguration()
            {
                return await context.Configuration.ToListAsync();
            }

            [HttpPost]
            public async Task<ActionResult<Configuration>> PostConfiguration()
            {
                Configuration config = new Configuration();
                config.Id = Request.Form["Id"];
                Console.WriteLine(config.Id);
                config.Type = Request.Form["type"];
                config.Class = Request.Form["Class"];
                config.Organization = Request.Form["Organization"];
                config.FeedbackConfig = Request.Form["FeedbackConfig"];
                config.DefaultLanguage = Request.Form["DefaultLanguage"];
                context.Configuration.Add(config);
                await context.SaveChangesAsync();

                return CreatedAtAction("GetConfiguration", new { id = config.Id }, config);
            }

            // GET: api/00u565e9jDDMZFNff4x6
            [HttpGet("{id}")]
            public async Task<ActionResult<Configuration>> GetConfigById(string id)
            {
                var feedback = await context.Configuration.FindAsync(id);

                if (feedback == null)
                {
                    return NotFound();
                }

                return feedback;
            }

            // UPDATE: api/configuration/update
            [HttpPost("update")]
            public async Task<IActionResult> UpdateConfig()
            {
                string id = Request.Form["Id"];
                Configuration config = new Configuration();
                config.Id = Request.Form["Id"];
                config.Type = Request.Form["type"];
                config.Class = Request.Form["Class"];
                config.Organization = Request.Form["Organization"];
                config.FeedbackConfig = Request.Form["FeedbackConfig"];
                config.DefaultLanguage = Request.Form["DefaultLanguage"];
                if (id != config.Id)
                    {
                        return BadRequest();
                    }

                context.Entry(config).State = EntityState.Modified;

                try
                {
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConfigExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();
            }

            private bool ConfigExists(string id)
            {
                return context.Configuration.Any(e => e.Id == id);
            }
        }
}
