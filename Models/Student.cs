﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CTSim.Models
{
    public partial class Student
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Sex { get; set; }
        public string ResidenceCountry { get; set; }
        public string Nationality { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string Ocupation { get; set; }
        public string Email { get; set; }
    }
}
