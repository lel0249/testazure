﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CTSim.Models
{
    public class Configuration
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Organization { get; set; }
        public string Class { get; set; }
        public string FeedbackConfig { get; set; }
        public string DefaultLanguage { get; set; }
    }
}
