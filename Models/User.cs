﻿using System;
using System.Collections.Generic;

namespace CTSim.Models
{
    public partial class User
    {
        public string Id { get; set; }
        public string Role { get; set; }
        public string Organization { get; set; }
        public string ClassName { get; set; }

    }
}
