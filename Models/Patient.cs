﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CTSim.Models
{
    public partial class Patient
    {
        public int Id { get; set; }
        public string StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Decription { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Sex { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
        public string Indication { get; set; }
        public string Question { get; set; }
        public string Type { get; set; }
        public string ExamType { get; set; }
    }
}
