﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CTSim.Models
{
    public class Exam
    {
        public int Id { get; set; }
        public int Protocol { get; set; }
        public string Description { get; set; }
        public string Indication { get; set; }
        public string Question { get; set; }
        public string Clinician { get; set; }
        public DateTime Date { get; set; }
        public string Comment { get; set; }
        public int OnwardReferral { get; set; }
        public int AssessmentType { get; set; }
        public bool Signed { get; set; }
        public bool Justified { get; set; }
        public bool ImagingProtocol { get; set; }
        public bool ImagingChecked { get; set; }
    }
}
