﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CTSim.Models
{
    public partial class ScanImage
    {
        public string Id { get; set; }
        public int Kv { get; set; }
        public string Mas { get; set; }
        public string ScanTime { get; set; }
        public decimal SliceThickness { get; set; }
        public string ScanFov { get; set; }
        public decimal CtdDose { get; set; }
        public decimal DlpDose { get; set; }
        public string ImageGroup { get; set; }
        public string ImageSeries { get; set; }
        public string ImageURL { get; set; }
    }
}
