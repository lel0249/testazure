﻿using CTSim.Models;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.EntityFrameworkCore;

namespace CTSim
{
    public partial class CTSimContext : DbContext
    {

        public CTSimContext()
        {
        }

        public CTSimContext(DbContextOptions<CTSimContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Patient> Patients { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<ScanImage> ScanImages { get; set; }
        public virtual DbSet<Exam> Exams { get; set; }

        public virtual DbSet<Configuration> Configuration { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Patient>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Birthdate).HasColumnType("date");

                entity.Property(e => e.Decription).HasMaxLength(50);

                entity.Property(e => e.StudentId).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Indication).HasMaxLength(255);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Postcode).HasMaxLength(25);

                entity.Property(e => e.Question).HasMaxLength(255);

                entity.Property(e => e.Sex).HasMaxLength(10);

                entity.Property(e => e.Type).HasMaxLength(20);

                entity.Property(e => e.ExamType).HasMaxLength(20);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsRequired();

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Organization)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ClassName)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Admin>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.FirstName).HasMaxLength(255);
                entity.Property(e => e.LastName).HasMaxLength(255);
                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasMaxLength(25);
                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100);
                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.Birthdate).HasColumnType("date");
                entity.Property(e => e.Sex).HasMaxLength(10);
                entity.Property(e => e.ResidenceCountry).HasMaxLength(50);
                entity.Property(e => e.Nationality).HasMaxLength(50);
                entity.Property(e => e.Address).HasMaxLength(255);
                entity.Property(e => e.Postcode).HasMaxLength(25);
                entity.Property(e => e.Ocupation).HasMaxLength(100);
                entity.Property(e => e.Email).HasMaxLength(255);
            });
            modelBuilder.Entity<ScanImage>(entity =>
            {
                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.Kv)
                    .IsRequired();
                entity.Property(e => e.Mas)
                    .IsRequired()
                    .HasMaxLength(50);
                entity.Property(e => e.ScanTime)
                    .IsRequired()
                    .HasMaxLength(50);
                entity.Property(e => e.SliceThickness)
                    .IsRequired()
                    .HasColumnType("decimal");
                entity.Property(e => e.ScanFov)
                    .IsRequired()
                    .HasMaxLength(50);
                entity.Property(e => e.CtdDose)
                    .IsRequired()
                    .HasColumnType("decimal"); ;
                entity.Property(e => e.DlpDose)
                    .IsRequired()
                    .HasColumnType("decimal"); ;
                entity.Property(e => e.ImageGroup)
                    .IsRequired()
                    .HasMaxLength(50);
                entity.Property(e => e.ImageSeries)
                    .IsRequired()
                    .HasMaxLength(50);
                entity.Property(e => e.ImageURL)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Exam>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
                entity.Property(e => e.Protocol)
                    .IsRequired();
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.Indication)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.Question)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.Clinician)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.Date)
                    .IsRequired()
                    .HasColumnType("date");
                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(255);
                entity.Property(e => e.OnwardReferral)
                    .IsRequired();
                entity.Property(e => e.AssessmentType)
                    .IsRequired();
                entity.Property(e => e.Signed)
                    .IsRequired()
                    .HasColumnType("bit");
                entity.Property(e => e.Justified)
                    .IsRequired()
                    .HasColumnType("bit");
                entity.Property(e => e.ImagingProtocol)
                    .IsRequired()
                    .HasColumnType("bit");
                entity.Property(e => e.ImagingChecked)
                    .IsRequired()
                    .HasColumnType("bit");

            });

            modelBuilder.Entity<Configuration>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();
                entity.Property(e => e.FeedbackConfig)
                    .IsRequired();
                entity.Property(e => e.Type)
                    .IsRequired();
                entity.Property(e => e.Class)
                    .IsRequired();
                entity.Property(e => e.Organization)
                    .IsRequired();
                entity.Property(e => e.DefaultLanguage)
                    .IsRequired();

            });
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

    }

}