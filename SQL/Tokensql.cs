﻿using CTSim.AuthRequirement;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CTSim.SQL
{
    public class Tokensql
    {
        private string Conf;
        private SqlConnection SqlCon;
        public Tokensql() 
        {
            Conf = "Server=localhost;Database=CTSim;Trusted_Connection=True;";
            SqlCon = new SqlConnection(Conf);
        }
        public void updateToken(string Id, string value)
        {
            SqlCon.Open();
            string SqlStr = "UPDATE Token SET token = '" + value + "' WHERE tokenId = '" + Id +"'";
            SqlCommand SqlCmd = new SqlCommand(SqlStr, SqlCon);
            SqlCmd.ExecuteNonQuery();
        }

        public void InsertToken(string Id,string value)
        {
            SqlCon.Open();
            string SqlStr = "INSERT INTO Token(tokenId, token) ";
            SqlStr += "VALUES('" + Id + "', '" + value + "')";
            Console.WriteLine(SqlStr);
            SqlCommand SqlCmd = new SqlCommand(SqlStr, SqlCon);
            SqlCmd.ExecuteNonQuery();
        }

        public string getToken(string Id)
        {
            string result; 
            SqlCon.Open();
            string SqlStr = "SELECT * FROM Token WHERE tokenId = '" + Id + "'";
            SqlCommand SqlCmd = new SqlCommand(SqlStr, SqlCon);
            SqlDataReader SqlData = SqlCmd.ExecuteReader();
            if (!SqlData.Read())
                result = "null";
            else
            {
                string token = SqlData["token"].ToString();
                result = token;
            }
            SqlCon.Close();
            return result;
        }

        public string getType(string Id)
        {
            string result;
            SqlCon.Open();
            string SqlStr = "SELECT * FROM Users WHERE Id = '" + Id + "'";
            SqlCommand SqlCmd = new SqlCommand(SqlStr, SqlCon);
            SqlDataReader SqlData = SqlCmd.ExecuteReader();
            if (!SqlData.Read())
                result = "null";
            else
            {
                string token = SqlData["Role"].ToString();
                result = token;
            }
            SqlCon.Close();
            return result;
        }

        public void newToken(string Id, string value) 
        {
            string ifExist = getToken(Id);
            if (ifExist == "null")
            {
                InsertToken(Id, value);
            }
            else
            {
                updateToken(Id, value);
            }
        }

        public bool testToken(string token)
        {
            string deCodeToken = Security.AESDecrypt(token);
            bool result = false;
            JObject tokenJson = (JObject)JsonConvert.DeserializeObject(deCodeToken);
            string userId = tokenJson["id"].ToString();
            string tokenText = tokenJson["token"].ToString();
            string dataToken = getToken(userId);
            if (dataToken.ToString().Trim() == tokenText.ToString()) 
            {
                result = true;
            }
            return result;
        }

        public string testRole(string token)
        {
            string deCodeToken = Security.AESDecrypt(token);
            JObject tokenJson = (JObject)JsonConvert.DeserializeObject(deCodeToken);
            string userId = tokenJson["id"].ToString();
            string userType = getType(userId);
            return userType;
        }
    }
}
