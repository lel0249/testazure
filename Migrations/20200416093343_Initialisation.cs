﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CTSim.Migrations
{
    public partial class Initialisation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Admins",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 255, nullable: false),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Exams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Protocol = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: false),
                    Indication = table.Column<string>(maxLength: 255, nullable: false),
                    Question = table.Column<string>(maxLength: 255, nullable: false),
                    Clinician = table.Column<string>(maxLength: 255, nullable: false),
                    Date = table.Column<DateTime>(type: "date", nullable: false),
                    Comment = table.Column<string>(maxLength: 255, nullable: false),
                    OnwardReferral = table.Column<int>(nullable: false),
                    AssessmentType = table.Column<int>(nullable: false),
                    Signed = table.Column<bool>(type: "bit", nullable: false),
                    Justified = table.Column<bool>(type: "bit", nullable: false),
                    ImagingProtocol = table.Column<bool>(type: "bit", nullable: false),
                    ImagingChecked = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Patients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 255, nullable: false),
                    LastName = table.Column<string>(maxLength: 255, nullable: false),
                    Decription = table.Column<string>(maxLength: 50, nullable: true),
                    Birthdate = table.Column<DateTime>(type: "date", nullable: true),
                    Sex = table.Column<string>(maxLength: 10, nullable: true),
                    Address = table.Column<string>(maxLength: 255, nullable: true),
                    Postcode = table.Column<string>(maxLength: 25, nullable: true),
                    Indication = table.Column<string>(maxLength: 255, nullable: true),
                    Question = table.Column<string>(maxLength: 255, nullable: true),
                    Type = table.Column<string>(maxLength: 20, nullable: true),
                    ExamType = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScanImages",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 255, nullable: false),
                    Kv = table.Column<int>(nullable: false),
                    Mas = table.Column<string>(maxLength: 50, nullable: false),
                    ScanTime = table.Column<string>(maxLength: 50, nullable: false),
                    SliceThickness = table.Column<decimal>(type: "decimal", nullable: false),
                    ScanFov = table.Column<string>(maxLength: 50, nullable: false),
                    CtdDose = table.Column<decimal>(type: "decimal", nullable: false),
                    DlpDose = table.Column<decimal>(type: "decimal", nullable: false),
                    ImageGroup = table.Column<string>(maxLength: 50, nullable: false),
                    ImageSeries = table.Column<string>(maxLength: 50, nullable: false),
                    ImageURL = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScanImages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(maxLength: 25, nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Password = table.Column<string>(maxLength: 100, nullable: false),
                    FirstName = table.Column<string>(maxLength: 255, nullable: false),
                    LastName = table.Column<string>(maxLength: 255, nullable: false),
                    Birthdate = table.Column<DateTime>(type: "date", nullable: true),
                    Sex = table.Column<string>(maxLength: 10, nullable: true),
                    ResidenceCountry = table.Column<string>(maxLength: 50, nullable: true),
                    Nationality = table.Column<string>(maxLength: 50, nullable: true),
                    Address = table.Column<string>(maxLength: 255, nullable: true),
                    Postcode = table.Column<string>(maxLength: 25, nullable: true),
                    Ocupation = table.Column<string>(maxLength: 100, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(maxLength: 255, nullable: false),
                    RoleCode = table.Column<int>(nullable: true),
                    GroupCode = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Users__1788CC4C9E1861E2", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Admins");

            migrationBuilder.DropTable(
                name: "Exams");

            migrationBuilder.DropTable(
                name: "Patients");

            migrationBuilder.DropTable(
                name: "ScanImages");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
