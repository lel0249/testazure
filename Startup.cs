using CTSim.AuthRequirement;
using CTSim.IdentityServer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using System;

namespace CTSim
{
    public class Startup
    {

        public Startup()
        {
        }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            SetupDatabase(services, environment);

            services.AddControllers();
            var builder = services.AddIdentityServer()
               .AddInMemoryApiResources(Config.Apis)
               .AddInMemoryClients(Config.Clients);
            builder.AddDeveloperSigningCredential();

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Admin", policy => policy.AddRequirements(new UserTypeRequirement("Admin")));
                options.AddPolicy("Organization", policy => policy.AddRequirements(new UserTypeRequirement("Organization")));
                options.AddPolicy("Teacher", policy => policy.AddRequirements(new UserTypeRequirement("Teacher")));
            });

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //services.AddAuthentication()
            //.AddCookie()
            //.AddOpenIdConnect();

            services.AddSingleton<IAuthorizationHandler, UserTypeAuthorizationHandler>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.WithOrigins("http://localhost:8080")

            .AllowAnyHeader()

            .AllowAnyMethod());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseIdentityServer();

            app.UseAuthorization();

            app.UseSession(); 

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

        private void SetupDatabase(IServiceCollection services, string environment)
        {
            if (environment == "Production")
            {
                string connectionString = Configuration.GetConnectionString("CTSim_DB");
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                services.AddDbContext<CTSimContext>(options =>
                options.UseSqlServer(connectionString)
                );
            }
            else
            {
                services.AddDbContext<CTSimContext>(options =>
                //optionsBuilder.UseSqlServer("Server=S669-58613;Database=CTSim;User=sa;Password=Passw0rd1;");
                //optionsBuilder.UseSqlServer("Server=localhost;Database=CTSim;Trusted_Connection=True;");
                //options.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Database=CTSim;"));
                //Lei test data
                options.UseSqlServer("Server=localhost;Database=CTSim;Trusted_Connection=True;"));
            }
        }
    }
}
