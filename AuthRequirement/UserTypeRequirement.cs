﻿using Microsoft.AspNetCore.Authorization;

namespace CTSim.AuthRequirement
{
    public class UserTypeRequirement : IAuthorizationRequirement
    {

        public string UserType { get; }

        public UserTypeRequirement(string userType)
        {
            UserType = userType;

        }
    }
}