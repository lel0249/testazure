﻿using CTSim.SQL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CTSim.AuthRequirement
{
    public class UserTypeAuthorizationHandler : AuthorizationHandler<UserTypeRequirement>
    {
        private  IHttpContextAccessor _httpContextAccessor;
        private Tokensql tokensql;
        public UserTypeAuthorizationHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            tokensql = new Tokensql();
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserTypeRequirement requirement)
        {
            var headers = _httpContextAccessor.HttpContext.Request.Headers;
            var token = headers["token"];
            var usertype = tokensql.testRole(token);

            bool result = false;
            switch (requirement.UserType)
            {
                case "Admin":
                    if (usertype == "SupAdmin" || usertype == "Admin")
                    {
                        result = true;
                    }
                    else {
                        result = false;
                    }
                    break;
                case "Organization":
                    if (usertype == "SupAdmin" || usertype == "Admin" || usertype == "Organization")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    break;
                case "Teacher":
                    if (usertype != "Student")
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    break;
            }

            if (result == true)
            {
                context.Succeed(requirement);
            }
            else {
                context.Fail();
            }
            Console.WriteLine(result.ToString());
            return Task.CompletedTask;
        }
    }
}
