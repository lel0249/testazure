﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CTSim.AuthRequirement
{
    public class HeaderInfo
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public HeaderInfo(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<string> GetHeaders()
        {
            var headers = _httpContextAccessor.HttpContext.Request.Headers;

            return headers["token"];
        }
    }
}
